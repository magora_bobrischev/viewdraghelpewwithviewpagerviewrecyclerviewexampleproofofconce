package com.serega.viewdraghelpertest;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class LayoutWithDraggableContentWithRecyclerView extends FrameLayout {
    private static final int H = Resources.getSystem().getDisplayMetrics().heightPixels;
    private ViewDragHelper vdh;
    private View draggingView;
    private RecyclerView recyclerView;

    public LayoutWithDraggableContentWithRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        vdh = ViewDragHelper.create(this, 1.0f, new ViewDragHelper.Callback() {

            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child.getId() == R.id.draggable_recycler_view_container
                        && (child.getTop() > 0 || !recyclerView.canScrollVertically(-1));
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                int topBound = getPaddingTop();
                int bottomBound = getHeight() - getPaddingBottom();

                return Math.min(Math.max(top, topBound), bottomBound);
            }

            @Override
            public void onViewReleased(View releasedChild, float xvel, float yvel) {
                if (yvel < 0) {
                    if (vdh.settleCapturedViewAt(0, 0)) {
                        ViewCompat.postInvalidateOnAnimation(LayoutWithDraggableContentWithRecyclerView.this);
                    }
                } else if (yvel > 0) {
                    if (vdh.settleCapturedViewAt(0, H / 2)) {
                        ViewCompat.postInvalidateOnAnimation(LayoutWithDraggableContentWithRecyclerView.this);
                    }
                } else {
                    if (releasedChild.getTop() < H / 4) {
                        if (vdh.settleCapturedViewAt(0, 0)) {
                            ViewCompat.postInvalidateOnAnimation(LayoutWithDraggableContentWithRecyclerView.this);
                        }
                    } else {
                        if (vdh.settleCapturedViewAt(0, H / 2)) {
                            ViewCompat.postInvalidateOnAnimation(LayoutWithDraggableContentWithRecyclerView.this);
                        }
                    }
                }
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return H / 2;
            }
        });
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (vdh != null && vdh.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int action = MotionEventCompat.getActionMasked(ev);
        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            vdh.cancel();
            return false;
        }
        return vdh.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        vdh.processTouchEvent(ev);
        return true;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        draggingView = findViewById(R.id.draggable_recycler_view_container);
        draggingView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                draggingView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewCompat.offsetTopAndBottom(draggingView, H / 2);
            }
        });
    }
}
