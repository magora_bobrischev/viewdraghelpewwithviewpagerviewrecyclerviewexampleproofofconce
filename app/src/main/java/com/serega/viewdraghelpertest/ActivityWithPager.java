package com.serega.viewdraghelpertest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public class ActivityWithPager extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_pager);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.view_pager);
        LayoutWithDraggableContentWithPager draggableContentWithPager = (LayoutWithDraggableContentWithPager) findViewById(R.id.draggable_content);
        Adapter adapter = new Adapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);
        draggableContentWithPager.setScrollConsumer(adapter);

    }

    private class Adapter extends FragmentStatePagerAdapter implements ScrollConsumer {
        private ScrollConsumer currentFragment;

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentWithRecyclerView.makeFragment();
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            currentFragment = (ScrollConsumer) object;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean consumeScroll(int direction) {
            return currentFragment != null && currentFragment.consumeScroll(direction);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Title #" + position;
        }
    }

}
