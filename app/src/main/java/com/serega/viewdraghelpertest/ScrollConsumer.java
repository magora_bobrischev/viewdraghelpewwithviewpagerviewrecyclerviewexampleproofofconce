package com.serega.viewdraghelpertest;

/**
 * @author S.A.Bobrischev
 *         Developed by Magora Team (magora-systems.com). 2016.
 */
public interface ScrollConsumer {

    boolean consumeScroll(int direction);
}
